import { Route, Switch,useParams,useLocation } from "react-router-dom";

export default function Main() {
  return (
    <div style={{ backgroundColor: 'white', width: '100%', height: '100%' }}>
      <Switch>
        <Route path="/menuCom">
            <MenuCom/>
        </Route>
        <Route path="/msgCom/:id">
            <MsgCom/>
        </Route>
        <Route path="/layoutCom">
            <LayoutCom/>
        </Route>
      </Switch>
    </div>
  )
}

function MenuCom(){
    const loc = useLocation()
    console.log(loc)
  return(
  <h1>MenuCom {loc.state.name}</h1>
  )
}

function MsgCom(){
    const params = useParams()
    console.log("id :"+params.id)
return <h1>MsgCom {params.id}</h1>
}

function LayoutCom(){
  return <h1>LayoutCom</h1>
}