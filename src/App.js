import './App.css';
import {BrowserRouter as Router} from 'react-router-dom'
import MyMenu from './Menu';
import Main from './Main';
import {Layout} from 'antd';

const {Header,Sider,Content,Footer} = Layout

export default function App(props){

  return(
    <Router>
      <Layout>
        <Header>

        </Header>
        <Layout>
          <Sider>
            <MyMenu/>
          </Sider>
          <Content>
            <Main/>
          </Content>
        </Layout>
        <Footer></Footer>
      </Layout>
    </Router>
      
    // <Router>
    //   <Menu></Menu>
    //   <Main></Main>
    // </Router>
    
  )
};