import {Menu} from 'antd'
import { useHistory } from 'react-router-dom'
import './Menu.css'

const {SubMenu,Item,ItemGroup} = Menu
export default function MyMenu(props) {
  const history = useHistory()

  function handerClick(e){
    console.log(e)
    const key = e.key;
    switch(key){
      case 'sub1-item1':
        history.push("/menuCom?size=10",{name:"biao"});
        break
      case 'sub1-item2':
        history.push("/msgCom/100");
        console.log('item2')
        break
      case 'sub1-item3':
        history.push("layoutCom");break;
      default:
        break
    }
  }

  return (
    <div>
      <Menu onClick={handerClick} mode="inline">
        <SubMenu key="sub1" title={<span>全部组件</span>}>
          <Item key="sub1-item1">菜单组件</Item>
          <Item key="sub1-item2">消息组件</Item>
          <Item key="sub1-item3">layout组件</Item>
          <Item key="sub1-item4">表单组件</Item>
        </SubMenu>
      </Menu>
    </div>
  )
}